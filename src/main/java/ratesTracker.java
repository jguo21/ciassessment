/**
 * @author Jack Guo
 *
 * 
 */
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class ratesTracker {
	
	static Map<String, LinkedList<Map<String, String>>> currRates ;
	
	public ratesTracker(){
		currRates = new HashMap<String, LinkedList<Map<String, String>>>();
	}
	//Instantiates the currRates database based on csv file and the number of lines the user would like to read to
	public static void instantiate(String csvfile, int maxNumLines) throws IOException{
		BufferedReader br = null;
		String csvseparator = ",";
		String line = "";
		try {
			br = new BufferedReader(new FileReader(csvfile));
			String headerLine = br.readLine();
			int l = 1;
			while((line = br.readLine()) != null && l <= maxNumLines){
				String[] rates = line.split(csvseparator);
				try{
				String baseCurr = rates[0];
				String counterCurr = rates[1];
				String exchRate = rates[2];
				if(baseCurr.equals("") || counterCurr.equals("") || exchRate.equals("")){
					throw new Exception();
				}
				Double testConvert = Double.parseDouble(exchRate);
				
				//System.out.println(baseCurr+", "+counterCurr);
				Map<String, String> counterCurrMap = new HashMap<String, String>();
				counterCurrMap.put(counterCurr, exchRate);
				LinkedList<Map<String, String>> counterCurrList = new LinkedList<Map<String, String>>();
				counterCurrList.add(counterCurrMap); 
				
				if(!currRates.containsKey(baseCurr)){
					currRates.put(baseCurr, counterCurrList);
				}else{
					currRates.get(baseCurr).get(0).put(counterCurr, exchRate);
				}
				//System.out.println(currRates);
				l++;
			}catch(Exception ex){
				//System.out.println("Bad formatting");
			}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("No such file");
		}
	
	}
	
	//Returns cross rate between two currencies
	public static String getCrossRate(String curr1, String curr2){
		DecimalFormat df = new DecimalFormat("#.####");
;		try{
			if(!currRates.containsKey(curr1) || !currRates.containsKey(curr2)){
				return "No such base currency in our data";
			}
			if(currRates.get(curr1).get(0).containsKey(curr2)){
				return currRates.get(curr1).get(0).get(curr2);
				}
			if(currRates.get(curr2).get(0).containsKey(curr1)){
				return currRates.get(curr2).get(0).get(curr1);
			}
			throw new Exception();
			
		}catch(Exception e){
			try{
				for(String key : currRates.get(curr1).get(0).keySet()){
					Double rate1 = Double.parseDouble(currRates.get(curr1).get(0).get(key));
					if(currRates.get(curr2).get(0).containsKey(key)){
						Double rate2 = Double.parseDouble(currRates.get(curr2).get(0).get(key));
						//System.out.println(rate1+" "+rate2);
						return df.format(rate1*rate2);
					}
				}
				return "No cross rate found";
			}catch(Exception ex){
				return "Cross rate error";
			}
		}	
	}
	
	//Returns the FX quote by first directly checking then checking cross rate 
	public static String getFXQuote(String curr1, String curr2){
		try{
			if(!(currRates.get(curr1).get(0).get(curr2)==null)){
			return currRates.get(curr1).get(0).get(curr2);}
			throw new Exception();
		}catch(Exception ex){
			return getCrossRate(curr1, curr2);
		}
	}
	
	/*public static void main(String[] args) throws IOException{
		String f = "test.csv";
		ratesTracker RT = new ratesTracker();
		RT.instantiate(f, 20);
		System.out.println(RT.currRates.get("AED"));
		System.out.println(RT.getCrossRate("AED", "ALL"));
	}*/

}
