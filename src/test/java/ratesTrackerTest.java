/**
 * @author Jack Guo
 *
 * 
 */
import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
/**
 * @author Jack Guo
 *
 * 
 */
public class ratesTrackerTest {
	String small_test;
	String corrupted_test;
	String full_file;
	
	@Before
	public void initiate(){
		small_test="test.csv";
		corrupted_test="corrupted_file.csv";
		full_file="rates.csv";
	}
	//Testing small csv
	@Test
	public void testSimpleGetRate() throws IOException{
		ratesTracker rt = new ratesTracker();
		rt.instantiate(small_test, 3);
		assertEquals("0.2663", rt.getFXQuote("AED", "AUD"));
		assertEquals("No such base currency in our data", rt.getFXQuote("AED", "AUX"));
	}
	
	@Test
	public void testRateUpdate() throws IOException{
		ratesTracker rt = new ratesTracker();
		rt.instantiate(small_test, 200);
		assertEquals("1.495", rt.getFXQuote("AED", "AUD"));
		assertEquals("0.2475", rt.getFXQuote("ARS", "CAD"));
	}
	
	@Test 
	public void testCrossRate() throws IOException{
		ratesTracker rt = new ratesTracker();
		rt.instantiate(small_test, 200);
		assertEquals("No such base currency in our data", rt.getFXQuote("LOL", "AUD"));
		assertEquals("0.2475", rt.getFXQuote("ARS", "CAD"));
		//System.out.println(String.format("%.5g%n", rt.getFXQuote("AED", "ARS")));
		assertEquals("0.3624", rt.getFXQuote("AED", "ARS"));
		assertEquals("No cross rate found", rt.getFXQuote("AED", "ALL"));
	}
	
	//Testing corrupted csv
	@Test
	public void testGetRateCorrupted() throws IOException{
		//System.out.println("===Testing Corrupt===");
		ratesTracker rt = new ratesTracker();
		rt.instantiate(corrupted_test, 200);
		assertEquals("1.495", rt.getFXQuote("AED", "AUD"));
		assertEquals("0.2475", rt.getFXQuote("ARS", "CAD"));
		//Cross rate
		assertEquals("12.9637", rt.getFXQuote("AED", "ARS"));
		//Non-numerical rates
		assertEquals("12.132", rt.getFXQuote("AED", "INR"));
		assertNotEquals("0.2707", rt.getFXQuote("AED", ""));
	}
	
	//Testing full file
	@Test
	public void testGetRateFull() throws IOException{
		ratesTracker rt = new ratesTracker();
		rt.instantiate(full_file, 500000);
		assertEquals("0.0014", rt.getFXQuote("ZMK", "ZAR"));
		assertNotEquals("1.495", rt.getFXQuote("AED", "AUD"));
		assertEquals("1.1727", rt.getFXQuote("ZAR","HKD"));

	}
	
}
